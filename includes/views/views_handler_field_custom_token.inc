<?php
/**
 * @file
 * Views custom text field handler.
 */

/**
 * A views field handler that allows tokens in custom text fields.
 */
class views_handler_field_custom_token extends views_handler_field_custom {

  function render_text($alter) {
    global $user;

    $text = parent::render_text($alter);
    $data = array(
      'user' => $user,
      'view' => $this->view,
    );
    // Return the text, so the code never thinks the value is empty.
    return token_replace($text, $data);
  }

}
