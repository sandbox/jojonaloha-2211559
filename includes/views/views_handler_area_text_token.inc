<?php
/**
 * @file
 * Definition of views_handler_area_text_token.
 */

/**
 * A views area handler that allows tokens in custom text areas.
 */
class views_handler_area_text_token extends views_handler_area_text {

  function render_textarea($value, $format) {
    global $user;

    $data = array(
      'user' => $user,
      'view' => $this->view,
    );

    // Return the text, so the code never thinks the value is empty.
    $value = token_replace($value, $data);
    return parent::render_textarea($value, $format);
  }
}
