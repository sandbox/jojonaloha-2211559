ABOUT VIEWS TOKEN HANDLERS
--------------------------

This is a sandbox project, which contains experimental code for developer use
only. Original sandbox project is located at:
http://drupal.org/sandbox/jojonaloha/2211559

Provides some custom Views handlers for custom text with support for tokens.

This module is partially inspired by #1417266: Using tokens in a view global
custom text field and the need for something like the Token Filter in more
places in Views.

Note: Some tokens may not work well with caching, so be mindful of which tokens
you use.
